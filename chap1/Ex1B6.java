import java.util.Scanner;

class Ex1B6{
	public static void main(String[] args){
		System.out.println("input 4 digits");
		Scanner s = new Scanner(System.in);
		int d = 0;
		while(d<1000 || d>9999) d = s.nextInt();
		Ex1B6.printTruss(d);
	}

	static void printTruss(int d){
		int i=1;
		do{
			System.out.println(i+" truss is "+d%10);
			d/=10;
			i++;
		}while(i<=4);
	}
}
