import java.util.Scanner;

class Ex1B3{
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		System.out.println("input base and height");
		double b = s.nextDouble();
		double h = s.nextDouble();
		double m = Ex1B3.calcTriangle(b, h);
		System.out.println("Triangle area is" + m);
	}

	public static double calcTriangle(double b, double h){
		return b*h/2;
	}


}
