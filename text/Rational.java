class Rational{
	private int mol;
	private int den;

	Rational(int mol, int den){
		if(den==0 || mol==0) throw new IllegalArgumentException("illigal argument");
		else{
			this.mol=mol;
			this.den=den;
		}
	}

	private int gcd(int i, int j){
		return j==0? i: gcd(j, i%j);
	}

	private set(int m, int d){
		den=d;
		mol=m;
	}

	int getMol(){
		return mol;
	}

	int getDen(){
		return den;
	}

	void add(Rational r){
		int m = mol*r.getDen()+r.getMol()*den;
		int d = den*r.getDen();
		int g = gcd(m, d);
		set(m/g, d/g);
	}

	void sub(Rational r){
		int m = mol*r.getDen()-r.getMol()*den;
		int d = den*r.getDen();
		int g = gcd(m, d);
		set(m/g, d/g);
	}

	void mul(Rational r){
		int m = mol*r.getMol();
		int d = den*getDen();
		int g = gcd(m,d);
		set(m/g, d/g);
	}

	void dev(Rational r){
		int m = mol*r.getDen();
		int d = den*r.getMol();
		int g = gcd(m,d);
		set(m/g, d/g);
	}

	void add(int i){
		return add(new Rational(i, 1));
	}

	void sub(int i){
		return sub(new Rational(i, 1));
	}

	void mul(int i){
		return mul(new Rational(i, 1));
	}

	void dev(int i){
		return dev(new Rational(i, 1));
	}
}
