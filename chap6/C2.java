import java.util.*;

class C2{

	public static void main(String[] args){
		int[] array = new int[20];
		randomSet(array, 1, 50);
		var li = new ArrayList<Integer>();
		for(int i : array) li.add(i);

		li.stream().filter((i)->{return (i%5)==0;})
			.sorted((a,b)->{return b-a;})
			.forEach((s)->{System.out.println(s);});
	}

	static void randomSet(int[] array, int foot, int head){
		Random r = new Random();
		head -= foot;
		for(int i=0; i<array.length; i++) array[i]=r.nextInt(head)+foot;
	}
}
