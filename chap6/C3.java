import java.util.*;

class C3{

	public static void main(String[] args){
		int[] array = new int[20];
		C2.randomSet(array, 0, 100);
		sort(array);
		System.out.println(array.toString());
	}

	static void sort(int[] array){
		int left=0, right=array.length-1, b;
		qsort(array, left, right);
	}

	static void qsort(int[] array, int left, int right){
		if(left<right){
			int i=left, j=right;
			int pivot = array[(left+right)/2];
			while(true){
				while(array[i]<pivot) i++;
				while(array[j]>pivot) j--;
				if(i<j) swap(array, i++, j--);
				else break;
			}
			qsort(array, left, i-1);
			qsort(array, j+1, right);
		}
	}

	static void swap(int[] array, int l, int r){
		int tmp = array[l];
		array[l]=array[r];
		array[r]=tmp;
	}


}
