
class Ex3A8{
	public static void main(String[] args){
		for(int i=0; i<=10; i++){
			try{
				System.out.println(i + "! = " + factrial(i));
			}catch(NotPositiveException e){
				System.err.println("positive error");
				continue;
			}
		}
	}

	static int factrial(int i) throws NotPositiveException {
		int res=1;
		if(i<=0) throw new NotPositiveException();
		else {
			for(;i>0; i--) res *= i;
		}
		return res;
	}
}

