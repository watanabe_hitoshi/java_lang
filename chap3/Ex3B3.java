class Ex3B3{

	public static void main(String[] args){
		for(int i=1; i<10; i++) printMultiTable(i);
	}

	static void printMultiTable(int i){
		for(int c=1, n=i; c<10; c++, n=i*c) System.out.print(n+" ");
		System.out.println("");
	}
}
