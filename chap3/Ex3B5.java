class Ex3B5{

	public static void main(String[] args)throws NotPositiveException{
		int i=5;
		int c=1;
		printDesParabolic(c, i);
		printAscParabolic(c, i);
	}

	static void printDesParabolic(int c, int i) throws NotPositiveException{
		if(i<=0) throw new NotPositiveException();
		else{
			for(;i>=0; i--){
 				for(int j=0;j<i*i*c;j++) System.out.print("●");	
				System.out.println("");
			}
		}
	}

	static void printAscParabolic(int c, int i) throws NotPositiveException{
		if(i<=0) throw new NotPositiveException();
		else{
			for(int n=0;n<=i; n++){
 				for(int j=0;j<n*n*c;j++) System.out.print("●");	
				System.out.println("");
			}
		}

	}
}
