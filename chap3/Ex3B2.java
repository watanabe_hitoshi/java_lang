import java.util.Scanner;

class Ex3B2{

	public static void main(String[] args){
		int i = new Scanner(System.in).nextInt();
		System.out.println("degit "+i+" is "+digit(i));
	}

	static int digit(int i){
		int res=0;
		for(;i>0;i/=10) res++;
		return res;
	}

}
