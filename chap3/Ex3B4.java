class Ex3B4{
	public static void main(String[] args) throws NotPositiveException{
		int i=4;
		printDesTriangle(i);
		printAscTriangle(i);
	}

	static void printDesTriangle(int i) throws NotPositiveException{
		if(i<=0) throw new NotPositiveException();
		else{
			for(;i>0; i--){
 				for(int j=0;j<i;j++) System.out.print("●");	
				System.out.println("");
			}
		}
	}

	static void printAscTriangle(int i) throws NotPositiveException{
		if(i<=0) throw new NotPositiveException();
		else{
			for(int c=1;c<=i; c++){
 				for(int j=0;j<c;j++) System.out.print("●");	
				System.out.println("");
			}
		}

	}
}
