import java.util.*;

class Ex3C2{
	
	public static void main(String[] args){
		Menu menu = new Menu();
		while(true){
			System.out.println("メニューを選んでください");
			menu.printMenu();
			System.out.print("選択(input number) -> ");
			int i = new Scanner(System.in).nextInt()-1;
			String picked = menu.getMenu(i);
			int size = menu.printContent(picked);
			System.out.println((size+1) + " : 別のメニューを選択する");
			System.out.println("選択(input number) ->");
			int j = new Scanner(System.in).nextInt()-1;
			if(j==size) continue;
			else{ 
				System.out.println(menu.getContent(picked, j) + "が選択されました");
				break;
			}
		}
	}


	private static class Menu{
		private List<String> menu = new ArrayList<String>();
		private List<String> bowls = new ArrayList<String>();
		private List<String> noodles = new ArrayList<String>();

		private Map<String, List<String>> content = new HashMap<String, List<String>>();

		Menu(){
			Collections.addAll(menu, "ご飯","麺");
			Collections.addAll(bowls, "A定食", "B定食");
			Collections.addAll(noodles, "ラーメン", "そば");
			content.put(menu.get(0), bowls);
			content.put(menu.get(1), noodles);
		}

		int printMenu(){
			int i=0;
			for(String s : menu)
				System.out.println(++i + " : " + s);
			return menu.size();
		}

		int printContent(String m){
			List<String> target;
			try{
				target = content.get(m);
			}catch (NullPointerException ex){
				System.out.println("dont has such a content");
				return 0;
			}

			int i=0;
			for(String s: target)
				System.out.println(++i+" : "+s);
			return target.size();
		}


		String getMenu(int i){
			return menu.get(i);
		}

		String getContent(String m, int i){
			return content.get(m).get(i);
		}
	}



}
