import java.util.Scanner;

class C2{
	public static void main(String[] args){
		int i=0,j=0;
		try{
			Scanner s=new Scanner(System.in);
			i=s.nextInt();
			j=s.nextInt();
		}catch(Exception e){
			e.printStackTrace();
		}

		System.out.println(gcd(i,j));
	}

	static int gcd(int i,int j){
		return (j==0)? i : gcd(j, i%j); 
	}

}
