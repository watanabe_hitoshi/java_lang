import java.util.*;

class B3{

	public static void main(String[] args){
		for(int i=1900; i<=2000; i++){
			if(isLeap(i)) System.out.println(i);
		}
	}

	static boolean isLeap(int i){
		return (i%400==0)? true: (i%4==0 && i%100!=0)? true: false;
	}

}

