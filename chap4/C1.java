import java.util.Scanner;

class C1{

	public static void main(String[] args){
		System.out.println("input number");
		int n=0;
		try{
			n = new Scanner(System.in).nextInt();
		}catch(Exception ex){
			ex.printStackTrace();	
		}
		try{
			System.out.printf("%s\n", isPrime(n) ? "素数です":"素数ではありません");
		}catch(NotPositiveException ex){
			System.out.println("inputed numer is not positive");
		}
	}

	static boolean isPrime(int n) throws NotPositiveException{
		if(n<=0) throw new NotPositiveException();
		else{
			for(int i=2; n>1 && i<(int)Math.sqrt(n); i++)
				if(n%i==0)
					return false;
			return true;
		}
	}
}
