class NotPositiveException extends MyException{
	public NotPositiveException(){
		super("argument is not positive");
	}
}
