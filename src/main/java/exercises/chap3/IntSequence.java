package exercises.chap3;

public interface IntSequence{
	
	boolean hasNext();
	int next();

	public static IntSequence of(int... a){
			return new IntSequence(){
					int i;
					public boolean hasNext(){ return i<a.length;}
					public int next(){ return a[i++]; }
			};
	}

	public static IntSequence constant(int a){
			return new IntSequence(){
					public boolean hasNext(){ return true; }
					public int next(){ return a;}
			};
	}
}
