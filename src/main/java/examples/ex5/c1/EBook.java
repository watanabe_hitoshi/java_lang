package examples.ex5.c1;

import examples.ex5.c1.Book;

public class EBook extends Book{
	private String format;

	public EBook(Book b, String format){
		super(b.title(), b.price());
		this.format=format;
	}

	public EBook(String title, int price , String format){
		super(title, price);
		this.format=format;
	}

	public String format(){
		return format;
	}

	public String format(String format){
		return (this.format=format);
	}
}
