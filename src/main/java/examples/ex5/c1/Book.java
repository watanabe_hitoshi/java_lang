package examples.ex5.c1;

public class Book{
	private String title;
	private int price;

	public Book(String title, int price){
			this.title=title;
			this.price=price;
	}

	public String title(){
		return title;
	}
	public String title(String title){
		return (this.title=title);
	}

	public int price(){
		return price;
	}
	public int price(int p){
		return (this.price=p);
	}
}
