package examples.ex5.a3;

public class BaseMember{
	private int id;
	private String name;

	public BaseMember(int id, String name){
		this.id=id;
		this.name=name;
	}

	public int id(){
		return id;
	}


	public String name(){
		return name;
	}	
	public String name(String name){
		return (this.name=name);
	}
}
