package examples.ex5.a3;
import examples.ex5.a3.BaseMember;

public class PointMember extends BaseMember{
	private int point;
			
	public PointMember(BaseMember b){
		super(b.id(), b.name());
	}
	public PointMember(BaseMember b, int point){
		super(b.id(), b.name());
		this.point=point;
	}

	public PointMember(int id, String name, int point){
		super(id, name);
		this.point = point;
	}

	public PointMember(int id, String name){
		super(id, name);
	}

	public int point(){
		return point;
	}

	public int point(int point){
		if(this.point+point<0) throw new IllegalArgumentException("less point");
		return (this.point += point);
	}
}
