package examples.math;
import java.util.Random;

/**
 * An <code>Rational</code> object represats an 有理数 
 * @author hitoshi watanabe
 */
public class Rational {

  private int numer;
  private int denom;
  private boolean fractable=true;
  private int hashCode;
  /**
   * initial block 約分
   */
  {
    fraction();
  }
  /**
   * 有理数を整数から生成
   * @param n the numerator
   */
  public Rational(int n) {
    this(n, 1);
  }
  /**
   * 有理数を分子分母から生成
   * @param n the numerator
   * @param d the denominator
   */
  public Rational(int n, int d) {
    if (d == 0)
      throw new IllegalArgumentException("Divisor must not be zero.");
    numer = n;
    denom = d;
  }
  /**
   * 分子を取得
   */
  public int numer() { fraction(); return numer; }
  /**
   * 分母を取得
   */
  public int denom() { fraction(); return denom; }
  /**
   * 約分を行う
   */
  public void fraction(){
    if(fractable){
		   	int g = gcd(numer, denom);
			numer /= g;
			denom /= g;
			hashCode = Integer.hashCode(numer) + 31*Integer.hashCode(denom);
	}
	fractable=false;
  }
  
  /**
   * 有理数の足し算
   * @param r the calculating rational
   */
  public void add(Rational r) {
    numer = numer*r.denom + r.numer*denom;
    denom *= r.denom;
	fractable=true;
  }
  /**
   * 有理数の引き算
   * @param r the calculating rational
   */
  public void subtract(Rational r) {
    numer = numer*r.denom - r.numer*denom;
    denom *= r.denom;
	fractable=true;
  }
  /**
   * 有理数の掛け算
   * @param r the calculating rational
   */
  public void multiply(Rational r) {
    numer *= r.numer;
    denom *= r.denom;
	fractable=true;
  }
  /**
   * 有理数の割り算
   * @param r the calculating rational
   */
  public void divide(Rational r) {
    multiply(new Rational(r.denom, r.numer));
	fractable=true;
  }
  /**
   * 整数との足し算
   * @param i the calculating integer
   */
  public void add(int i)      { add(new Rational(i));         }
  /**
   * 整数との引き算
   * @param i the calculating integer
   */
  public void subtract(int i) { subtract(new Rational(i));    }
  /**
   * 整数との掛け算
   * @param i the calculating integer
   */
  public void multiply(int i) { multiply(new Rational(i));    }
  /**
   * 整数との割り算
   * @param i the calculating integer
   */
  public void divide(int i)   { multiply(new Rational(1, i)); }

  public String toString() {
    int d = denom();
    return d == 1 ? Integer.toString(numer()) : numer() + "/" + d;
  }

  private int gcd(int a, int b) {
    return b == 0 ? a : gcd(b, a%b);
  }
  
    /**
	 * 引数の有理数と同じ有理数かどうか判定
	 * @param o the compair object
	 */
	@Override
	public boolean equals(Object o){
		if(o == this)
			return true;
		if(o instanceof Rational){
			Rational r = (Rational)o;
			return r.hashCode()==hashCode();
		}else 
			return false;
	}
	/**
	 * ハッシュ値を生成
	 */
	@Override
	public int hashCode(){
		fraction();
		return hashCode;
	}
}

