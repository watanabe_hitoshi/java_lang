package examples.ex8.b1;

import java.util.Scanner;
import java.util.InputMismatchException;
public class ExceptionEx8_B1{
	public static void main(String[] args){
		int[] a={1,2,3,4,5};
		System.out.println("インデックスを入力してください");
		Scanner s = new Scanner(System.in);
		while(true){
			try{
				int i = s.nextInt();
				int e = a[i];
				System.out.println("s[" +i+"]="+e);
				break;
			}catch(InputMismatchException ex){
				System.out.println("input integer");
				s.next();
			}catch(ArrayIndexOutOfBoundsException ex){
				System.out.println("input 0-4");
			}
		}

	}
}
