package examples.ex8.a1;

import java.util.Scanner;
import java.util.InputMismatchException;
public class ExceptionEx8_A1{
	public static void main(String[] args){
		int[] a={1,2,3,4,5};
		System.out.println("インデックスを入力してください");
		Scanner s = new Scanner(System.in);
		try{
			show(s, a);		
		}catch(InputMismatchException e){
			System.out.println("inpupt integer");
			s = s.reset();
		}catch(ArrayIndexOutOfBoundsException e){
			System.out.println("input 0 - 4");
		}finally{
			show(s,a);
		}
	}

	private static void show(Scanner s, int[] a){
		while(true){
				int i = s.nextInt();
				int e = a[i];
				System.out.println("s[" +i+"]="+e);
				break;
		}

	}
}
