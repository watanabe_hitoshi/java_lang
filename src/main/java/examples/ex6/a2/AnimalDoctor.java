package examples.ex6.a2;
import examples.ex6.a2.Doctor;

public class AnimalDoctor extends Doctor{
		AnimalDoctor(String name){
				super(name);
		}

		@Override
		public void examine(){
				System.out.println("動物を診査します");
		}
}
