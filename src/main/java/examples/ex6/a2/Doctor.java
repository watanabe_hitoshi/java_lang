package examples.ex6.a2;

public abstract class Doctor{
	private String name;

	public Doctor(String name){
		name(name);
	}

	public String name(){
		return name;
	}

	public void name(String name){
		if(name.equals("")) throw new IllegalArgumentException("unrecognize name");
		this.name=name;
	}

	public abstract void examine();
}
