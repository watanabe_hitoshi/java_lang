package examples.ex6.c34;
import static examples.ex6.c34.Result.*;
import static examples.ex6.c34.Hand.*;

abstract class Player{
	protected String name;
	Player(String name){
		this.name=name;
	}

	@Override
	public String toString(){
			return name;
	}

	abstract public Hand selectHand();

}
