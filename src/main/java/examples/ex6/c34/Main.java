package examples.ex6.c34;
import examples.ex6.c34.*;
import static examples.ex6.c34.Hand.*;
import static examples.ex6.c34.Result.*;

public class Main{
	public static void main(String[] args) throws Exception{
		Player p1 = new HumanPlayer("hitoshi");
		ComputerPlayer p2 = new ComputerPlayer(new RandomStrategy());

		for(int i=0; i<10; i++){
			var p1r = pon(p1);
			selected(p1r);
			var p2r = pon(p2);
			selected(p2r);
			var res = p1r.judge(p2r);
			System.out.println(switch(res){	
				case WIN->  p1.toString()+" win!";
				case DRAW-> "DRAW";
				case LOSE-> p2.toString()+" win!";
				default ->  throw new Exception("invalid result");
			});
			if(i==5) p2.strategy(new STBStrategy(p1r));
			if(i>6) p2.preHand(p1r);
		}
	}


	private static Hand pon(Player p){
		template();
		return p.selectHand();
	}

	private static void template(){
		System.out.println("0 for グー, 1 for チョキ, 2 for パー");
	}

	private static void selected(Hand h){
			System.out.println(h.toString());
	}


	private void showResult(Result res){
	}
}
