package examples.ex6.c34;

import examples.ex6.c34.*;
import static examples.ex6.c34.Hand.*;
import java.util.Random;

public class RandomStrategy implements Strategy{

		@Override
		public Hand getHand(){
			Random r = new Random();
			return (Hand.values())[r.nextInt(3)];
		}

		@Override
		public void setOpponentHand(Hand h){
				System.out.println("not neccesary");
		}
}
