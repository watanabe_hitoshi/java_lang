package examples.ex6.c34;

import examples.ex6.c34.*;
import static examples.ex6.c34.Hand.*;
import static examples.ex6.c34.Result.*;

public class STBStrategy implements Strategy{
		private Hand preh;

		public STBStrategy(Hand h){
				setOpponentHand(h);
		}

		@Override
		public Hand getHand(){
			for(int i=0; i<Hand.values().length; i++){
					Hand h = (Hand.values())[i];
					if(h.judge(preh)==WIN) return h;
			}
			return null;
		}


		@Override
		public void setOpponentHand(Hand h){
				preh = h;
		}
}
