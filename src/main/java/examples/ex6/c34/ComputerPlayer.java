package examples.ex6.c34;
import examples.ex6.c34.*;
import static examples.ex6.c34.Hand.*;
import static examples.ex6.c34.Result.*;

class ComputerPlayer extends Player{

	private Strategy s;

	ComputerPlayer(Strategy s){
		super("cpu");
		strategy(s);
	}

	@Override
	public Hand selectHand(){
		return s.getHand();
	}

	public void strategy(Strategy s){
			this.s = s;
	}

	public void preHand(Hand h){
		s.setOpponentHand(h);
	}
}

