package examples.ex6.c34;
import examples.ex6.c34.Player;
import static examples.ex6.c34.Hand.*;
import static examples.ex6.c34.Result.*;
import java.util.Scanner;
import java.util.InputMismatchException;

class HumanPlayer extends Player{
	HumanPlayer(String name){
		super(name);
	}

	@Override
	public Hand selectHand(){
		var s = new Scanner(System.in);
		while(true){
			try{
				int i = s.nextInt();
				return (Hand.values())[i];
			}catch (InputMismatchException ex){
				s.next();
				System.out.println("one more");
			}catch(ArrayIndexOutOfBoundsException ex){
				System.out.println("one more");
			}
		}
	}
}
