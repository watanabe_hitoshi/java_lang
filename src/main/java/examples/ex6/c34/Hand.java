package examples.ex6.c34;
import examples.ex6.c34.*;
import static examples.ex6.c34.Result.*;

public enum Hand implements Umpire{
	
	ROCK("グー"){
		@Override public Result judge(Hand enemy){
			return enemy==this? DRAW:(enemy==PAPER)? LOSE: WIN; 
		}
	},
	SCISSOR("チョキ"){
		@Override public Result judge(Hand enemy){
			return enemy==this? DRAW:(enemy==ROCK)? LOSE: WIN; 
		}
	},
	PAPER("パー"){
		@Override public Result judge(Hand enemy){
			return enemy==this? DRAW:(enemy==SCISSOR)? LOSE: WIN; 
		}
	},
	;

	private final String name;
	Hand(String name){
		this.name=name;
	}
	@Override
	public String toString(){
		return name;
	}
}
