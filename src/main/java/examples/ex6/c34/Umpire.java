package examples.ex6.c34;
import static examples.ex6.c34.Result.*;
import static examples.ex6.c34.Hand.*;

abstract interface Umpire{
	abstract Result judge(Hand enemy);
}
