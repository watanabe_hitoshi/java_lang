package examples.ex6.c34;

import examples.ex6.c34.*;
import static examples.ex6.c34.Hand.*;

public interface Strategy{
		public Hand getHand();
		public void setOpponentHand(Hand h);
}
