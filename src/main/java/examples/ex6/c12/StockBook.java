package examples.ex6.c12;
import examples.ex6.c12.Book;

public class StockBook extends Book{
	private int stock;

	public StockBook(Book b, int stock){
		super(b.itemNo(), b.title(), b.price());
		stock(stock);
	}

	public StockBook(int itemNo, String title, int price , int stock){
		super(itemNo, title, price);
		stock(stock);
	}

	public int stock(){
			return stock;
	}
	public void stock(int s){
			if(stock+s<0) throw new IllegalArgumentException("invalid stock");
			stock+=s;
	}
}
