package examples.ex6.c12;

public abstract class Book{
	protected final int itemNo;
	protected String title;
	protected int price;
	protected boolean canMultiBuy;

	{
		this.canMultiBuy = true;
	}

	protected Book(int itemNo, String title, int price){
			if(itemNo<0) throw new IllegalArgumentException("invalid id");
			this.itemNo=itemNo;
			this.title=title;
			price(price);
	}

	public String title(){
		return title;
	}
	public void title(String title){
		this.title=title;
	}

	public int price(){
		return price;
	}
	public void price(int p){
		price=p;
	}
	public int itemNo(){
		return itemNo;
	}

	protected String collect(){
			return itemNo+ ". "+title + "-- price:" + price();
	}
	@Override
	public String toString(){
			return collect();
	}
}
