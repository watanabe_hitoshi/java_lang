package examples.ex6.c12;
import examples.ex6.c12.Book;

public class MPBook extends Book{
	private int postage;
	private String exhibitor;

	public MPBook(Book b, int postage, String ex){
		super(b.itemNo(), b.title(), b.price());
		postage(postage);
		exhibitor(ex);
	}

	public MPBook(int itemNo, String title, int price , int postage, String ex){
		super(itemNo, title, price);
		postage(postage);
		exhibitor(ex);
	}

	public String exhibitor(){
		return exhibitor;
	}
	public void exhibitor(String ex){
		if(ex.equals("")) throw new IllegalArgumentException("invalid exhibitor");
		exhibitor=ex;
	}

	public int postage(){
		return postage;
	}
	public void postage(int pos){
		if(pos<0) throw new IllegalArgumentException("invalid postage");
		postage=pos;
	}
}
