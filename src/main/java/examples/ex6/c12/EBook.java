package examples.ex6.c12;
import examples.ex6.c12.Book;

public class EBook extends Book{
	private String format;

	public EBook(Book b, String format){
		super(b.itemNo(), b.title(), b.price());
		canMultiBuy=false;
		this.format=format;
	}

	public EBook(int itemNo, String title, int price , String format){
		super(itemNo, title, price);
		this.format=format;
	}

	public String format(){
		return format;
	}

	public String format(String format){
		return (this.format=format);
	}
	@Override
	public String toString(){
			return collect() + "\n(eBook)format : "+format(); 
	}
}
