package examples.ex6.a3;
import examples.ex6.a3.Doctor;

public class AnimalDoctor implements Doctor{
	String name;
	
	AnimalDoctor(String name){
		name(name);
	}
	
	@Override
	public void name(String name){
		if(name.equals("")) throw new IllegalArgumentException("unrecognize name");
		this.name=name;
	}

	@Override
	public String name(){
			return name;
	}

	@Override
	public void examine(){
		System.out.println("動物を診査します");
	}
}
