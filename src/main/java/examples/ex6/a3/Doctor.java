package examples.ex6.a3;

public interface Doctor{

	String name();
	void name(String name);

	default public void examine(){
		System.out.println("診察します");
	}
}
