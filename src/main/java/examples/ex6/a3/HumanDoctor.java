package examples.ex6.a3;
import examples.ex6.a3.Doctor;

class HumanDoctor implements Doctor{
	private String name;
	
	HumanDoctor(String name){
		name(name);
	}
	
	@Override
	public void name(String name){
		if(name.equals("")) throw new IllegalArgumentException("unrecognize name");
		this.name=name;
	}

	@Override
	public String name(){
			return name;
	}


	@Override
	public void examine(){
			System.out.println("人を診察します");
	}
}
