package examples.ex6.a1;
import examples.ex6.a1.Doctor;

class HumanDoctor extends Doctor{
	HumanDoctor(String name){
			super(name);
	}

	@Override
	public void examine(){
			System.out.println("人を診察します");
	}
}
