package examples.collection;

public class Pair<T1,T2>{
	private T1 buf1;
	private T2 buf2;

	Pair(T1 mem1, T2 mem2){
			this.buf1 = mem1;
			this.buf2 = mem2;
	}

	public T1 buf1(){
		return buf1;
	}

	public T2 buf2(){
		return buf2;
	}
}
