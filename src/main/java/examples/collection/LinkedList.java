package examples.collection;

import java.util.ArrayList;
import java.util.function.Predicate;

public class LinkedList<T> {
	private final LinkedList<T> next;
	private final T element;
	private boolean nil=false;
	
	public LinkedList(T element,LinkedList<T> next) {
		if(element==null && next==null) nil = true;
		this.element=element;
		this.next=next;
	}
	
	public T head() {
		return element;
	}
	
	public LinkedList<T> tail(){
		return next;
	}
	
	
	public boolean isNil() {
		return nil;
	}
	
	public int length() {
		return isNil()? 1 : 1 + next.length();
	}
	
	public T find(Predicate<? super T> filter) {
		return isNil()? null : filter.test(head())? head() : next.find(filter);
	}
	
	public ArrayList<T> filter(Predicate<? super T> filter){
		var res = new ArrayList<T>();
		for(LinkedList<T> now=this; !now.isNil(); now=now.tail())
			res.add(now.head());
		return res;
	}
	
	
	
	
	
}

