package text.chap15;
import  java.util.function.*;
import java.util.Random;

public interface InfiniteIntSequence{
	default public boolean hasNext(){ return true; }
	public int next();


	public static InfiniteIntSequence generate(IntSupplier s){
		return s::getAsInt;
	}

	public static InfiniteIntSequence iterate(int seed, IntUnaryOperator f){
		return new InfiniteIntSequence(){
			int lseed = seed;
			@Override public int next(){
				int res=lseed;
				lseed=f.applyAsInt(lseed);
				return res;
			}
		};
	}

	public static InfiniteIntSequence random(int from, int until){
		Random r = new Random();
		int bound = until-from;
		if(0<bound) return ()->r.nextInt(bound)+from;
		else throw new IllegalArgumentException();
	}

	public static InfiniteIntSequence range(int from){
		return iterate(from, i -> i+1 );
	}

	default public void forEach(IntConsumer c, int n){
		while(n-- > 0 && hasNext() ) c.accept(next());
	}
}
