package text.chap15;

import java.util.function.*;
import java.util.NoSuchElementException;

public interface IntSequence{
	boolean hasNext();
	int next();

	static IntSequence range(int from, int untill){
		return new IntSequence(){
			private int i=from;
			@Override
			public boolean hasNext(){
				   	return i < untill;
			}
			
			@Override
			public int next(){
				   if(hasNext()) return i++;
				   else throw new NoSuchElementException();
			}
		};
	}

	default void forEach(IntConsumer c){
		while(hasNext()) c.accept(next());
	}
}
