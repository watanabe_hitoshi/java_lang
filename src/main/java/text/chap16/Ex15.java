package text.chap16;
import java.util.ArrayList;
import java.util.function.*;

public class Ex15{
		public static <T, R> ArrayList<R> map(ArrayList<T> array, Function<? super T, ? extends R> func){
			var res = new ArrayList<R>();
			for(T e: array)
				res.add(func.apply(e));
			return res;
		} 
}
