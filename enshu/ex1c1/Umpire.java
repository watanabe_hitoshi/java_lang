class Umpire{
	static int judge(int p1, int p2){
		int res = (p1-p2+3)%3;
		return (res==2)? -1 : res;
	}
}
