import java.util.Random;

class ComputerPlayer extends Player{
	ComputerPlayer(){
		super("cpu");
	}

	@Override
	int selectHand(){
		template();
		return new Random().nextInt(3)+1;
	}
}

