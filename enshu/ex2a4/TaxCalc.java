class TaxCalc{
	double rate = 0.08;

	int bill(int price){
		return price + (int)(price*rate);
	}

	int bill(int price, double rate){
		return price + (int)(price*rate);
	}

	int bill(int price, double rate, int discount){
		return (price-discount) + (int)((price-discount)*rate);
	}
}
