class Bank{
	private int stock;

	void deposit(int money){
		stock += money;
	}

	void drawer(int money) throws Exception{
		if(stock<money) throw new Exception("not enough");
		stock -= money;
	}

	void stockView(){
		System.out.println(stock);
	}
}
