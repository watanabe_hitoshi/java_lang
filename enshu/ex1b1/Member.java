class Member{
	int id;
	String name;

	Member(int id, String name){
		this.id=id;
		this.name=name;
	}

	void introduce(){
		System.out.printf("%10s%10s\n", "id", "name");
		System.out.printf("%10d%10s\n", id, name);
	}
}
