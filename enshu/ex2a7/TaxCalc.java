class TaxCalc{
	double rate;

	TaxCalc(){
		this.rate=0.08;
	}

	Taxcalc(int y){
		if(y<=1988) this.rate=0;
		else if(y<=1997) this.rate=0.03;
		else if(y<=2014) this.rate=0.05;
		else TaxCalc();
	}

	int bill(int price){
		return price + (int)(price*rate);
	}

	int bill(int price, double rate){
		return price + (int)(price*rate);
	}

	int bill(int price, double rate, int discount){
		return (price-discount) + (int)((price-discount)*rate);
	}
}
