public class Product{
	private final int code;
	private String name;
	private int price;

	public Product(int c, String n, int p){
		this.code=c;
		this.name=n;
		price(p);
	}

	public int price(int price){
		if(price<0) throw new IllegalArgumentException("invalid price");
		return (this.price=price);
	}

	public int price(){
		return price;
	}

	public String name(){
		return name; 
	}

	public String name(String name){
		return (this.name=name);
	}

	public int code(){
		return code;
	}

	public String toString(){
		return code + ": " + name + "->" + price;
	}
}
